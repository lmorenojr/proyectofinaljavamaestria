package com.up.maestria.vacunacion;

import java.awt.Color;

public final class Configuration {
	public static final String DATABASE_PATH = "C:/git/proyectofinaljavamaestria/database/DATOS.accdb";
	public static final int WIDTH = 1024;
	public static final int HEIGHT = 768;
	public static final Color SAVE_BTN_COLOR = new Color(92, 184, 92);
	public static final Color CANCEL_BTN_COLOR = Color.LIGHT_GRAY;
}
