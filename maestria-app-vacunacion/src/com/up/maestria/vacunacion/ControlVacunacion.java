package com.up.maestria.vacunacion;

import java.awt.EventQueue;

import com.up.maestria.vacunacion.ui.screens.AppContainerScreen;

public class ControlVacunacion {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new AppContainerScreen();
			}
		});
	}

}
