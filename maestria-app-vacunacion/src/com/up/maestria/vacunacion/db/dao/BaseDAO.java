package com.up.maestria.vacunacion.db.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.up.maestria.vacunacion.Configuration;

public abstract class BaseDAO {
	private Connection connection;

	private String connectionString() {
		return String.format("jdbc:ucanaccess://%s", Configuration.DATABASE_PATH);
	}
	
	protected Connection getConnection() {
		Connection connection = null;
		try {
	        System.out.println("Estableciendo conexion");
	        Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
	        connection = DriverManager.getConnection(connectionString());
	        System.out.println("Se realizo la conexion de la base de datos");
	    } catch (Exception sqle) {
	        System.out.println(sqle);
	    }
		return connection;
	}
	
	protected void printSQLException(SQLException ex) {
		for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
