package com.up.maestria.vacunacion.db.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.up.maestria.vacunacion.db.model.Paciente;

public class PacienteDAO extends BaseDAO {
	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final String SELECT_USER_BY_ID = "SELECT * from DATOS WHERE ID =?";
	private static final String SELECT_USER_BY_CEDULA = "SELECT * from DATOS WHERE CEDULA =?";
    
	 private static final String UPDATE_USERS_SQL = "UPDATE DATOS set NOMBRE = ?,APELLIDO= ?, EMAIL =?, CEDULA =?, TELEFONO =?, DIRECCION =? " + 
	 ", PROVINCIA =?, FECHA_NACIMIENTO =?, TIPO_VACUNA =?, SEXO =? " +
	 ", FECHA_PRIMERA_DOSIS =?, PRIMERA_DOSIS_APLICADA =?, PRIMERA_DOSIS_SIN_SINTOMAS =? " +
	 ", FECHA_SEGUNDA_DOSIS =?, SEGUNDA_DOSIS_APLICADA =?, SEGUNDA_DOSIS_SIN_SINTOMAS =? " +
	 ", COMENTARIOS =? where ID = ?";

	private static final String INSERT_PACIENTE_SQL = "INSERT INTO DATOS" + "  (NOMBRE, APELLIDO, EMAIL, CEDULA, TELEFONO, DIRECCION, PROVINCIA, FECHA_NACIMIENTO, TIPO_VACUNA, SEXO, FECHA_PRIMERA_DOSIS) VALUES " +
	        " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

	private static final String SELECT_USER_BY_DATE_AND_VACUNA = "SELECT * from DATOS WHERE TIPO_VACUNA =? AND FECHA_PRIMERA_DOSIS between ? AND ? ORDER BY CEDULA";
 
	public List<Paciente> buscarPacientesDelDiaPorTipoDeVacuna(String tipoDeVacuna) {
		List < Paciente > pacientes = new ArrayList < > ();
        
		try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_DATE_AND_VACUNA);) {
			preparedStatement.setString(1, tipoDeVacuna);
			preparedStatement.setDate(2, Date.valueOf(LocalDate.now()));
			preparedStatement.setDate(3, Date.valueOf(LocalDate.now()));
			
			ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                pacientes.add(convertirResultSetAPaciente(rs));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return pacientes;
	}
	public Paciente buscarPacientePorId(int id) {
        try (Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                return convertirResultSetAPaciente(rs);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return null;
    }
	
	public Paciente buscarPacientePorCedula(String cedula) {
        try (Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_CEDULA);) {
            preparedStatement.setString(1, cedula);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                return convertirResultSetAPaciente(rs);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return null;
    }
	
	public void insertar(Paciente paciente) {
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PACIENTE_SQL)) {
            preparedStatement.setString(1, paciente.getNombre());
            preparedStatement.setString(2, paciente.getApellido());
            preparedStatement.setString(3, paciente.getEmail());
            preparedStatement.setString(4, paciente.getCedula());
            preparedStatement.setString(5, paciente.getTelefono());
            preparedStatement.setString(6, paciente.getDireccion());
            preparedStatement.setString(7, paciente.getProvincia());
            preparedStatement.setString(8, paciente.getFechaDeNacimientoFormatted());
            preparedStatement.setString(9, paciente.getTipoDeVacuna());
            preparedStatement.setString(10, paciente.getSexo());
            preparedStatement.setString(11, paciente.getFechaDePrimeraDosisFormatted());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }
	 
	public boolean actualizarPaciente(Paciente paciente) {
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USERS_SQL);) {
        	 preparedStatement.setString(1, paciente.getNombre());
             preparedStatement.setString(2, paciente.getApellido());
             preparedStatement.setString(3, paciente.getEmail());
             preparedStatement.setString(4, paciente.getCedula());
             preparedStatement.setString(5, paciente.getTelefono());
             preparedStatement.setString(6, paciente.getDireccion());
             preparedStatement.setString(7, paciente.getProvincia());
             preparedStatement.setString(8, paciente.getFechaDeNacimientoFormatted());
             preparedStatement.setString(9, paciente.getTipoDeVacuna());
             preparedStatement.setString(10, paciente.getSexo());
             preparedStatement.setString(11, paciente.getFechaDePrimeraDosisFormatted());
             preparedStatement.setBoolean(12, paciente.isPrimeraDosisAplicada());
             preparedStatement.setBoolean(13, paciente.isPrimeraDosisNoSintomas());
             preparedStatement.setString(14, paciente.getFechaDeSegundaDosisFormatted());
             preparedStatement.setBoolean(15, paciente.isSegundaDosisAplicada());
             preparedStatement.setBoolean(16, paciente.isSegundaDosisNoSintomas());
             preparedStatement.setString(17, paciente.getComentarios());
             preparedStatement.setInt(18, paciente.getId());
             return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
        	 printSQLException(e);
        	 return false;
		}
    }
	   
	protected Paciente convertirResultSetAPaciente(ResultSet rs) {
		Paciente paciente = new Paciente();
		try {
			paciente.setId(rs.getInt("ID"));
			paciente.setNombre(rs.getString("NOMBRE"));
			paciente.setApellido(rs.getString("APELLIDO"));
			paciente.setCedula(rs.getString("CEDULA"));
			paciente.setTelefono(rs.getString("TELEFONO"));
			paciente.setDireccion(rs.getString("DIRECCION"));
			paciente.setProvincia(rs.getString("PROVINCIA"));
			paciente.setFechaDeNacimiento(fromDateStringToLocalDate(rs.getString("FECHA_NACIMIENTO")));
			paciente.setTipoDeVacuna(rs.getString("TIPO_VACUNA"));
			paciente.setEmail(rs.getString("EMAIL"));
			paciente.setSexo(rs.getString("SEXO"));
			
			paciente.setFechaDePrimeraDosis(fromDateStringToLocalDate(rs.getString("FECHA_PRIMERA_DOSIS")));
			paciente.setPrimeraDosisAplicada(rs.getBoolean("PRIMERA_DOSIS_APLICADA"));
			paciente.setPrimeraDosisNoSintomas(rs.getBoolean("PRIMERA_DOSIS_SIN_SINTOMAS"));
			
			paciente.setFechaDeSegundaDosis(fromDateStringToLocalDate(rs.getString("FECHA_SEGUNDA_DOSIS")));
			paciente.setSegundaDosisAplicada(rs.getBoolean("SEGUNDA_DOSIS_APLICADA"));
			paciente.setSegundaDosisNoSintomas(rs.getBoolean("SEGUNDA_DOSIS_SIN_SINTOMAS"));
			
			paciente.setComentarios(rs.getString("COMENTARIOS"));
		} catch (SQLException e) {
			printSQLException(e);
		}
		return paciente;
	}
	
	private LocalDate fromDateStringToLocalDate(String dateString) {
		if (dateString == null || dateString.isEmpty()) {
			return null;
		}
		try {
			java.util.Date date = DATE_FORMATTER.parse(dateString);
			return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
