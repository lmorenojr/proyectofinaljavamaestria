package com.up.maestria.vacunacion.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO extends BaseDAO {
	private final String LOGIN_QUERY_TEMPLATE = "SELECT COUNT(ID) as rowcount FROM USUARIOS WHERE [USER] = '%s' AND PASS = '%s'";
	
	public boolean login(String username, String password) {
		try (Connection connection = getConnection()) {
			String query = String.format(LOGIN_QUERY_TEMPLATE, username, password);
			PreparedStatement statement = connection.prepareStatement(query);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			int count = resultSet.getInt("rowcount");
			resultSet.close();
			statement.close();
			return count == 1;
		} catch (SQLException exception) {
			printSQLException(exception);
			return false;
		}
	}
}
