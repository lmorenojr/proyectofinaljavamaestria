package com.up.maestria.vacunacion.db.model;

import java.time.LocalDate;

public class Paciente {
	private Integer id;
	private String nombre;
	private String apellido;
	private String email;
	private String sexo;
	private String cedula;
	private String telefono;
	private String direccion;
	private String provincia;
	private LocalDate fechaDeNacimiento;
	private String tipoDeVacuna;
	private LocalDate fechaDePrimeraDosis;
	private boolean primeraDosisAplicada;
	private boolean primeraDosisNoSintomas;
	private LocalDate fechaDeSegundaDosis;
	private boolean segundaDosisAplicada;
	private boolean segundaDosisNoSintomas;
	private String comentarios;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	public String getTipoDeVacuna() {
		return tipoDeVacuna;
	}
	public void setTipoDeVacuna(String tipoDeVacuna) {
		this.tipoDeVacuna = tipoDeVacuna;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public LocalDate getFechaDePrimeraDosis() {
		return fechaDePrimeraDosis;
	}
	public void setFechaDePrimeraDosis(LocalDate fechaDePrimeraDosis) {
		this.fechaDePrimeraDosis = fechaDePrimeraDosis;
	}
	public boolean isPrimeraDosisAplicada() {
		return primeraDosisAplicada;
	}
	public void setPrimeraDosisAplicada(boolean primeraDosisAplicada) {
		this.primeraDosisAplicada = primeraDosisAplicada;
	}
	public boolean isPrimeraDosisNoSintomas() {
		return primeraDosisNoSintomas;
	}
	public void setPrimeraDosisNoSintomas(boolean primeraDosisNoSintomas) {
		this.primeraDosisNoSintomas = primeraDosisNoSintomas;
	}
	public LocalDate getFechaDeSegundaDosis() {
		return fechaDeSegundaDosis;
	}
	public void setFechaDeSegundaDosis(LocalDate fechaDeSegundaDosis) {
		this.fechaDeSegundaDosis = fechaDeSegundaDosis;
	}
	public boolean isSegundaDosisAplicada() {
		return segundaDosisAplicada;
	}
	public void setSegundaDosisAplicada(boolean segundaDosisAplicada) {
		this.segundaDosisAplicada = segundaDosisAplicada;
	}
	public boolean isSegundaDosisNoSintomas() {
		return segundaDosisNoSintomas;
	}
	public void setSegundaDosisNoSintomas(boolean segundaDosisNoSintomas) {
		this.segundaDosisNoSintomas = segundaDosisNoSintomas;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	public String getFechaDeNacimientoFormatted() {
		return fechaDeNacimiento.toString();
	}
	
	public String getFechaDePrimeraDosisFormatted() {
		if(fechaDePrimeraDosis != null) {
			return fechaDePrimeraDosis.toString();
		}
		return null;
	}
	public String getFechaDeSegundaDosisFormatted() {
		if(fechaDeSegundaDosis != null) {
			return fechaDeSegundaDosis.toString();
		}
		return null;
	}
}
