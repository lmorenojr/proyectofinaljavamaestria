package com.up.maestria.vacunacion.ui.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Function;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class AppMenuBar extends JMenuBar implements ActionListener {
	private JMenu mfile=new JMenu("Archivo");
	private JMenuItem mnew=new JMenuItem("Nuevo paciente");
	private JMenuItem msearch=new JMenuItem("Buscar por cedula");
	private JMenuItem mexit=new JMenuItem("Salir");
	private JMenu mhelp=new JMenu("Ayuda");
	private JMenuItem maboutus=new JMenuItem("Acerca del App");
	private Function<Void, Void> onExit;
	private Function<Void, Void> onAddNew;
	private Function<Void, Void> onQuickSearch;
	
	public AppMenuBar(Function<Void, Void> onAddNew, Function<Void, Void> onExit, Function<Void, Void> onQuickSearch) {
		setup();
		this.onAddNew = onAddNew;
		this.onExit = onExit;
		this.onQuickSearch = onQuickSearch;
	}

	private void setup() {
		mfile.add(mnew);
		mfile.add(msearch);
		msearch.addActionListener(this);
        mfile.add(mexit);
        mnew.addActionListener(this);
        mexit.addActionListener(this);
        
        mhelp.add(maboutus);
        maboutus.addActionListener(this);
        
        add(mfile);
        add(mhelp);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Nuevo paciente")) {
			this.onAddNew.apply(null);
		}
		if (e.getActionCommand().equals("Buscar por cedula")) {
			this.onQuickSearch.apply(null);
		}
		if (e.getActionCommand().equals("Salir")) {
			this.onExit.apply(null);
		}
		if (e.getActionCommand().equals("Acerca del App")) {
			JOptionPane.showMessageDialog(this,
	                "Universidad de Panama. Profesora: Denis Cedeno. 2021. \nPresentado por Lina Carvajal, Luis Espino, Leonardo Moreno y Yazmin Perez.",
	                "Sistema de Control Vacunacion Covid-19",
	                JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
