package com.up.maestria.vacunacion.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import java.lang.Void;

import com.up.maestria.vacunacion.db.dao.PacienteDAO;
import com.up.maestria.vacunacion.db.model.Paciente;

public class CreatePatientDialog extends JDialog {
	
	private PacienteDAO dao = new PacienteDAO();
	private PatientForm patientForm;
	private Function<Void, Void> onSave;
	
	public CreatePatientDialog(Frame frame, Function<Void, Void> onSave) {
		super(frame);
		this.onSave = onSave;
		setupUI();
	}

	private void setupUI() {
		setTitle("Agregar nuevo paciente");
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setResizable(false);
		setSize(450, 480);
		setLocationRelativeTo(this.getParent());
		
		patientForm = new PatientForm(false, this.addNewPatientHandler, this.closeHandler);
		add(patientForm, BorderLayout.NORTH);
		
		setVisible(true);
	}
	
	private Function<Paciente, Void> addNewPatientHandler = (paciente) -> {
		dao.insertar(paciente);
		this.dispose();
		JOptionPane.showMessageDialog(this,
			  "Usuario creado exitosamente.",
              null,
              JOptionPane.INFORMATION_MESSAGE);
		this.onSave.apply(null);
    	return null;
    };
    
    private Function<Void, Void> closeHandler = (Void) -> {
    	this.dispose();
    	return Void;
    };
}
