package com.up.maestria.vacunacion.ui.components;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.function.Function;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.up.maestria.vacunacion.Configuration;
import com.up.maestria.vacunacion.db.model.Paciente;

public class PatientForm extends JPanel implements ActionListener {
	private static final String DATE_FORMAT_REGEX = "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$";
	
	private JTextField inputName = new JTextField();
	private JTextField inputApellido = new JTextField();
	private JTextField inputCedula = new JTextField();
	private JTextField inputFechaNacimiento = new JTextField();
	private JComboBox inputSexo = new JComboBox(new String[]{"M","F"});
    
	private JTextField inputEmail = new JTextField();
	private JTextField inputTelefono = new JTextField();
	private JComboBox inputVacuna = new JComboBox(new String[]{"PFIZER","ASTRA-ZENECA"});
	private JTextField inputDireccion = new JTextField();
	private JComboBox inputProvincia = new JComboBox(new String[]{
				"BOCAS DEL TORO",
				"COCLE",
				"COLON",
				"CHIRIQUI",
				"DARIEN",
				"HERRERA",
				"LOS SANTOS",
				"PANAMA",
				"VERAGUAS",
				"GUNA YALA",
				"PANAMA OESTE"
			}
	);
	private JTextField inputFechaPrimeraVacuna = new JTextField();
	private JCheckBox inputPrimeraVacunaAplicada = new JCheckBox();
	private JCheckBox inputPrimeraVacunaNoSintomas = new JCheckBox();
	
	private JTextField inputFechaSegundaVacuna = new JTextField();
	private JCheckBox inputSegundaVacunaAplicada = new JCheckBox();
	private JCheckBox inputSegundaVacunaNoSintomas = new JCheckBox();
	private JTextArea inputComentarios = new JTextArea();
	
	private JButton saveButton = new JButton("Guardar");
	private JButton cancelButton = new JButton("Cancelar");

	private boolean isEditMode;
	private Function<Paciente, Void> onSave;
	private Function<Void, Void> onClose;
	
	public PatientForm(boolean isEditMode, Function<Paciente, Void> onSave, Function<Void, Void> onClose) {
		this.isEditMode = isEditMode;
		this.onSave = onSave;
		this.onClose = onClose;
		setupUI();
	}

	private void setupUI() {
		int rowCount = this.isEditMode ? 19 : 13;
		GridLayout layout = new GridLayout(rowCount, 2);
		layout.setHgap(10);
		layout.setVgap(5);
		setLayout(layout);
		addComponents();
	}
	
	protected void addComponents() {
		//Header
		add(new Label("* Requerido"));
		add(new Label(""));
		// Fila 1
		add(new Label("* Nombre:"));
		add(inputName);
		// Fila 2
		add(new Label("* Apellido:"));
		add(inputApellido);
		// Fila 3
		add(new Label("* Fecha de Nacimiento (YYYY-mm-dd):"));
		add(inputFechaNacimiento);
		// Fila 4
		add(new Label("*Cedula:"));
		add(inputCedula);
		// Fila 5
		add(new Label("Sexo:"));
		add(inputSexo);
		// Fila 6
		add(new Label("Email:"));
		add(inputEmail);
		// Fila 7
		add(new Label("* Telefono:"));
		add(inputTelefono);
		// Fila 8
		add(new Label("* Direccion:"));
		add(inputDireccion);
		// Fila 9
		add(new Label("Provincia:"));
		add(inputProvincia);
		// Fila 10
		add(new Label("Tipo de Vacuna:"));
		add(inputVacuna);
		
		add(new Label("Fecha primera vacuna (YYYY-mm-dd):"));
		add(inputFechaPrimeraVacuna);
		if (this.isEditMode) {		
			add(new Label("Primera vacuna, aplicada?"));
			add(inputPrimeraVacunaAplicada);
			
			add(new Label("Primera vacuna, No presenta sintomas?"));
			add(inputPrimeraVacunaNoSintomas);
			
			add(new Label("Fecha segunda vacuna (YYYY-mm-dd):"));
			add(inputFechaSegundaVacuna);
			
			add(new Label("Segunda vacuna, aplicada?"));
			add(inputSegundaVacunaAplicada);
			
			add(new Label("Segunda vacuna, No presenta sintomas?"));
			add(inputSegundaVacunaNoSintomas);
			
			add(new Label("Comentarios: "));
			add(inputComentarios);
		}
		//Save Row
		saveButton.addActionListener(this);
		saveButton.setBackground(Configuration.SAVE_BTN_COLOR);
		add(saveButton);
		cancelButton.addActionListener(this);
		cancelButton.setBackground(Configuration.CANCEL_BTN_COLOR);
		add(cancelButton);
	}

	public void prepopulateFieldsForEdit(Paciente paciente) {
		if (paciente != null) {
			inputName.setText(paciente.getNombre());
			inputApellido.setText(paciente.getApellido());
			inputFechaNacimiento.setText(paciente.getFechaDeNacimientoFormatted());
			inputCedula.setText(paciente.getCedula());
			inputSexo.setSelectedItem(paciente.getSexo());
			inputTelefono.setText(paciente.getTelefono());
			inputEmail.setText(paciente.getEmail());
			inputDireccion.setText(paciente.getDireccion());
			inputProvincia.setSelectedItem(paciente.getProvincia());
			
			inputVacuna.setSelectedItem(paciente.getTipoDeVacuna());
			inputFechaPrimeraVacuna.setText(paciente.getFechaDePrimeraDosisFormatted());
			inputPrimeraVacunaAplicada.setSelected(paciente.isPrimeraDosisAplicada());
			inputPrimeraVacunaNoSintomas.setSelected(paciente.isPrimeraDosisNoSintomas());
			
			inputFechaSegundaVacuna.setText(paciente.getFechaDeSegundaDosisFormatted());
			inputSegundaVacunaAplicada.setSelected(paciente.isSegundaDosisAplicada());
			inputSegundaVacunaNoSintomas.setSelected(paciente.isSegundaDosisNoSintomas());
			
			inputComentarios.setText(paciente.getComentarios());
			
			inputCedula.setEditable(false);
		}
	}
	
	private boolean isValidForm() {
		boolean allRequiredFieldsSet = true;
		if(!validateRequiredTextInput(inputName)) {
			allRequiredFieldsSet = false;
		}
		if(!validateRequiredTextInput(inputApellido)) {
			allRequiredFieldsSet = false;
		}
		if(!validateRequiredTextInput(inputFechaNacimiento)) {
			allRequiredFieldsSet = false;
		}
		if(!validateRequiredTextInput(inputCedula)) {
			allRequiredFieldsSet = false;
		}
		if(!validateRequiredTextInput(inputTelefono)) {
			allRequiredFieldsSet = false;
		}
		if(!validateRequiredTextInput(inputDireccion)) {
			allRequiredFieldsSet = false;
		}
		boolean allDateFieldProperlyFormatted = true;
		if (!validateTextInputDateFormat(inputFechaNacimiento)) {
			allDateFieldProperlyFormatted = false;
		}
		if(!inputFechaPrimeraVacuna.getText().isEmpty() && !validateTextInputDateFormat(inputFechaPrimeraVacuna)) {
			allDateFieldProperlyFormatted = false;
		}
		if(!inputFechaSegundaVacuna.getText().isEmpty() && !validateTextInputDateFormat(inputFechaSegundaVacuna)) {
			allDateFieldProperlyFormatted = false;
		}
		return allRequiredFieldsSet && allDateFieldProperlyFormatted;
	}
	
	private boolean validateTextInputDateFormat(JTextField field) {
		boolean matches = field.getText().matches(DATE_FORMAT_REGEX);
		if (matches) {
			try {
				LocalDate.parse(field.getText());
			}catch(DateTimeParseException exception) {
				matches = false;
			}
		}
		if (!matches) {
			field.setBorder(BorderFactory.createLineBorder(Color.red));
		} else {
			field.setBorder(null);
		}
		return matches;
	}
	
	private boolean validateRequiredTextInput(JTextField field) {
		if (field.getText() == null || field.getText().isEmpty()) {
			field.setBorder(BorderFactory.createLineBorder(Color.red));
			return false;
		}
		field.setBorder(null);
		return true;
	}
	
	private Paciente getDataFromForm() {
		Paciente paciente = new Paciente();
		paciente.setNombre(inputName.getText());
		paciente.setApellido(inputApellido.getText());
		paciente.setFechaDeNacimiento(LocalDate.parse(inputFechaNacimiento.getText()));
		paciente.setCedula(inputCedula.getText());
		paciente.setSexo(inputSexo.getSelectedItem().toString());
		paciente.setEmail(inputEmail.getText());
		paciente.setTelefono(inputTelefono.getText());
		paciente.setDireccion(inputDireccion.getText());
		paciente.setProvincia(inputProvincia.getSelectedItem().toString());
		paciente.setTipoDeVacuna(inputVacuna.getSelectedItem().toString());
		
		if  (!inputFechaPrimeraVacuna.getText().isEmpty()) {
			paciente.setFechaDePrimeraDosis(LocalDate.parse(inputFechaPrimeraVacuna.getText()));
			paciente.setPrimeraDosisAplicada(inputPrimeraVacunaAplicada.isSelected());
			paciente.setPrimeraDosisNoSintomas(inputPrimeraVacunaNoSintomas.isSelected());
		}
		
		if  (!inputFechaSegundaVacuna.getText().isEmpty()) {
			paciente.setFechaDeSegundaDosis(LocalDate.parse(inputFechaSegundaVacuna.getText()));
			paciente.setSegundaDosisAplicada(inputSegundaVacunaAplicada.isSelected());
			paciente.setSegundaDosisNoSintomas(inputSegundaVacunaNoSintomas.isSelected());
		}
		paciente.setComentarios(inputComentarios.getText());
		return paciente;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Guardar")) {
			if (this.isValidForm()) {
				this.onSave.apply(this.getDataFromForm());
			}
		}
		if (e.getActionCommand().equals("Cancelar")) {
			int response = JOptionPane.showConfirmDialog(this, "Perdera los cambios que no haya salvado.", "Esta seguro que desea cancelar?", JOptionPane.YES_NO_OPTION);
			if(response == 0) {
				this.onClose.apply(null);
			}
		}
	}
}
