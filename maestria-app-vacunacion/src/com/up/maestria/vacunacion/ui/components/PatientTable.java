package com.up.maestria.vacunacion.ui.components;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.up.maestria.vacunacion.db.dao.PacienteDAO;
import com.up.maestria.vacunacion.db.model.Paciente;

public class PatientTable extends JPanel {
	private JTable table;
	private PacienteDAO dao = new PacienteDAO();
	private String tipoDeVacuna;
	private Function<String, Void> onEditRow;
	private MouseAdapter clickAdapter = new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
	        int rowIndex = table.getSelectedRow();
	        String cedula = table.getModel().getValueAt(rowIndex, 2).toString();
	        handleRowClick(cedula);
		}
	};
	
	public PatientTable(String tipoDeVacuna, Function<String, Void> onEditRow) {
		this.tipoDeVacuna = tipoDeVacuna;
		this.onEditRow = onEditRow;
		setupGui();
	}
	
	protected void handleRowClick(String cedula) {
		this.onEditRow.apply(cedula);
	}

	private DefaultTableModel buildTableModel() {
		String headers[] = { "Nombre", "Apellido", "Cedula", "Dosis 1 Fecha", "Dosis 1 Aplicada", "Dosis 1 OK",  "Dosis 2 Fecha", "Dosis 2 Aplicada", "Dosis 2 OK" };
		Vector<String> columnNames = new Vector<String>(Arrays.asList(headers));
		Vector rows = new Vector();
		List<Paciente> pacientes = dao.buscarPacientesDelDiaPorTipoDeVacuna(tipoDeVacuna);
		pacientes.forEach(paciente -> {
			Vector newRow = new Vector();
			newRow.addElement(paciente.getNombre());
			newRow.addElement(paciente.getApellido());
			newRow.addElement(paciente.getCedula());
			newRow.addElement(paciente.getFechaDePrimeraDosisFormatted());
			newRow.addElement(paciente.isPrimeraDosisAplicada());
			newRow.addElement(paciente.isPrimeraDosisNoSintomas());
			newRow.addElement(paciente.getFechaDeSegundaDosisFormatted());
			newRow.addElement(paciente.isSegundaDosisAplicada());
			newRow.addElement(paciente.isSegundaDosisNoSintomas());
			
			rows.add(newRow);
		});
		 return new DefaultTableModel(rows, columnNames);
	}
	
	private void setupGui() {
		setLayout(new BorderLayout());
		table = new JTable(buildTableModel());
		table.setFillsViewportHeight(true);
		table.addMouseListener(clickAdapter);
		
		add(new JScrollPane(table), BorderLayout.CENTER);
		setVisible(true);
	}

	public void refresh() {
		this.table.setModel(buildTableModel());
	}
	
}
