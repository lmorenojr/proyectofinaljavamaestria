package com.up.maestria.vacunacion.ui.components;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.up.maestria.vacunacion.db.dao.PacienteDAO;
import com.up.maestria.vacunacion.db.model.Paciente;

public class UpdatePatientDialog extends JDialog {

	private PacienteDAO dao = new PacienteDAO();
	private PatientForm patientForm;
	private Paciente paciente;
	private Function<Void, Void> onUpdate;
	
	public UpdatePatientDialog(Frame frame, Paciente paciente, Function<Void, Void> onUpdate) {
		super(frame);
		this.paciente = paciente;
		this.onUpdate = onUpdate;
		setupUI();
	}

	private void setupUI() {
		setTitle("Editando paciente " + paciente.getCedula());
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setResizable(false);
		setSize(450, 640);
		setLocationRelativeTo(this.getParent());
		
		patientForm = new PatientForm(true, this.addNewPatientHandler, this.closeHandler);
		patientForm.prepopulateFieldsForEdit(paciente);
		add(patientForm, BorderLayout.NORTH);
		
		setVisible(true);
	}
	
	private Function<Paciente, Void> addNewPatientHandler = (paciente) -> {
		paciente.setId(this.paciente.getId());
		dao.actualizarPaciente(paciente);
		this.dispose();
		this.onUpdate.apply(null);
		JOptionPane.showMessageDialog(this,
			  "Usuario actualizado exitosamente.",
              null,
              JOptionPane.INFORMATION_MESSAGE);
    	return null;
    };
    
    private Function<Void, Void> closeHandler = (Void) -> {
    	this.dispose();
    	return Void;
    };
           
}
