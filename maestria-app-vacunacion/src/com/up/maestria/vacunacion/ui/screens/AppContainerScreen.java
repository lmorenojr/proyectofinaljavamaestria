package com.up.maestria.vacunacion.ui.screens;

import java.awt.CardLayout;
import java.awt.event.WindowEvent;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.up.maestria.vacunacion.Configuration;
import com.up.maestria.vacunacion.db.dao.PacienteDAO;
import com.up.maestria.vacunacion.db.model.Paciente;
import com.up.maestria.vacunacion.ui.components.AppMenuBar;
import com.up.maestria.vacunacion.ui.components.CreatePatientDialog;
import com.up.maestria.vacunacion.ui.components.UpdatePatientDialog;

public class AppContainerScreen extends JFrame {
	public static final String MAIN_PANEL = "main";
    public static final String LOGIN_PANEL = "login";
    
    private JPanel contentPane;
    private PantallaPrincipal mainScreen;
    private PantallaLogin loginScreen;
	private AppMenuBar customAppMenu;
	private PacienteDAO dao = new PacienteDAO();
	
    // Event / Action Handlers
    private Function<Void, Void> loginSuccessHandler = (Void) -> {
    	setTitle("Sistema de Control Vacunacion Covid-19");
    	
    	CardLayout cl = (CardLayout) (contentPane.getLayout());
        cl.show(contentPane, MAIN_PANEL);	
        setJMenuBar(customAppMenu);
		return Void;
    };
    
    private Function<Void, Void> exitHandler = (Void) -> {
    	dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		return Void;
    };
    
    private Function<Void, Void> addNewPatientHandler = (Void) -> {
    	new CreatePatientDialog(this, this.handlePatientChangeHandler);
		return Void;
    };
    
    private Function<Void, Void> handlePatientChangeHandler = (Void) -> {
    	this.mainScreen.refresh();
		return Void;
    };
    
    private Function<Void, Void> searchByCedulaHandler = (Void) -> {
    	 String cedula = (String)JOptionPane.showInputDialog(
             this,
             "Busqueda De Paciente", 
             "Introduzca cedula (con guiones)",            
             JOptionPane.PLAIN_MESSAGE,
             null,            
             null, 
             null
          );
          if(cedula != null && cedula.length() > 0){
             Paciente paciente = dao.buscarPacientePorCedula(cedula);
             if (paciente == null) {
            	 JOptionPane.showMessageDialog(this,
                         "Intente nuevamente",
                         "No se encontraron registros.",
                         JOptionPane.ERROR_MESSAGE);
             } else {
            	 new UpdatePatientDialog(this, paciente, this.handlePatientChangeHandler);
             }
          }
		return Void;
    };
    
    private Function<String, Void> editByCedulaHandler = (cedula) -> {
    	Paciente paciente = dao.buscarPacientePorCedula(cedula);
    	new UpdatePatientDialog(this, paciente, this.handlePatientChangeHandler);
    	return null;
    };
    
	public AppContainerScreen() {
		setTitle("Iniciar Sesion - Sistema de Control Vacunacion Covid-19");
		setSize(Configuration.WIDTH, Configuration.HEIGHT);
        setLocationRelativeTo(null);
        setResizable(false);

        setGui();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
	}
	
	 private void setGui() {
	     try {
	    	 //App Menu
	    	 customAppMenu = new AppMenuBar(this.addNewPatientHandler, this.exitHandler, this.searchByCedulaHandler);
	    	 //Card Layout to display custom screens
	    	 contentPane = new JPanel(new CardLayout());
	         setContentPane(contentPane);
	         //Setting main screens
	         loginScreen = new PantallaLogin(this.loginSuccessHandler);
	         mainScreen = new PantallaPrincipal(this.editByCedulaHandler);
	         //Adding screens to main container
	         contentPane.add(loginScreen, LOGIN_PANEL);
	         contentPane.add(mainScreen, MAIN_PANEL);
	     } catch (Exception e) {
	         e.printStackTrace();
	     }
	 }
}
