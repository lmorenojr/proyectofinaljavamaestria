package com.up.maestria.vacunacion.ui.screens;

import javax.swing.*;

import com.up.maestria.vacunacion.Configuration;
import com.up.maestria.vacunacion.db.dao.UserDAO;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Callable;
import java.util.function.Function;

public class PantallaLogin extends JPanel implements ActionListener {

	private UserDAO userDao = new UserDAO();
    private JLabel userLabel=new JLabel("Usuario:");
    private JLabel passwordLabel=new JLabel("Contrasena:");
    private JTextField userTextField=new JTextField("");
    private JPasswordField passwordField=new JPasswordField("");
    private JButton loginButton=new JButton("Entrar");
    private JButton resetButton=new JButton("Cancelar");
    private Function<Void, Void> onLoginSuccess;
    
    private void inicializarComponentes() {
        setLayout(new BorderLayout());
        JLabel background=new JLabel(new ImageIcon(this.getClass().getResource("loginBackgroundAz.jpeg")));
        add(background);

        userLabel.setBounds(300,150,100,30);
        userTextField.setBounds(400,150,200,30);

        passwordLabel.setBounds(300,200,100,30);
        passwordField.setBounds(400,200,200,30);

        loginButton.setBounds(350,240,100,30);
        loginButton.setBackground(Configuration.SAVE_BTN_COLOR);
        loginButton.addActionListener(this);
        resetButton.setBounds(470,240,100,30);
        resetButton.addActionListener(this);
        resetButton.setBackground(Configuration.CANCEL_BTN_COLOR);

        background.add(userLabel);
        background.add(passwordLabel);
        background.add(userTextField);
        background.add(passwordField);
        background.add(loginButton);
        background.add(resetButton);
    }
    
    public PantallaLogin(Function<Void, Void> onLoginSuccess) {
    	this.onLoginSuccess = onLoginSuccess;
        inicializarComponentes();
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	if (e.getActionCommand().equals("Entrar")) {
            String username = userTextField.getText();
            String password = passwordField.getText();
            
            if (userDao.login(username, password)) {
                try {
					this.onLoginSuccess.apply(null);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
            } else {
                JOptionPane.showMessageDialog(this,
                        "Intente nuevamente",
                        "Usuario y/o Contrasena Incorrecta",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    	
        if (e.getActionCommand().equals("Cancelar")) {
            userTextField.setText(null);
            passwordField.setText(null);
        }
    }
}

