package com.up.maestria.vacunacion.ui.screens;

import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.up.maestria.vacunacion.ui.components.AppMenuBar;
import com.up.maestria.vacunacion.ui.components.PatientTable;

public class PantallaPrincipal extends JPanel {
	
	private JTabbedPane tabs = new JTabbedPane();
	private Function<String, Void> editByCedulaHandler;
	private PatientTable tablaPfizer;
	private PatientTable tablaAstraZeneca;

	public PantallaPrincipal(Function<String, Void> editByCedulaHandler) {
		this.editByCedulaHandler = editByCedulaHandler;
		setupGui();
	}

	private void setupGui() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		tablaPfizer = new PatientTable("ASTRA-ZENECA", editByCedulaHandler);
		tabs.addTab("Pacientes del Dia (ASTRA-ZENECA)", tablaPfizer);
		
		tablaAstraZeneca = new PatientTable("PFIZER", editByCedulaHandler);
		tabs.addTab("Pacientes del Dia (PFIZER)", tablaAstraZeneca);
		
		add(tabs);
		setVisible(true);
	}
	
	public void refresh() {
		tablaAstraZeneca.refresh();
		tablaPfizer.refresh();
	}
}
